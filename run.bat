@echo off
set algo_path=%~dp0

set algo_save_dir=%algo_path%saved\
set algo_output_dir=%algo_path%output\
set algo_input_dir=%algo_path%input\

if not exist "%algo_input_dir%" (
	mkdir "%algo_input_dir%"
	echo Created "input" dir
)
if not exist "%algo_output_dir%" (
	mkdir "%algo_output_dir%"
	echo Created "output" dir
)
if not exist "%algo_save_dir%" (
	mkdir "%algo_save_dir%"
	echo Created "save" dir
)

echo Launching algobench
java -jar %algo_path%algobench.jar %algo_path% %algo_path%algobench_b.exe
echo Exited