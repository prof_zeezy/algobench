/*
 * The MIT License
 *
 * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package inf2b.algobench.model;

import inf2b.algobench.main.AlgoBench;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * Represents a task to be given to the backend. TODO: Make Task abstract or an
 * interface, and create sub-classes: SortTask, GraphTask, SearchTask, HashTask
 *
 * @author eziama ubachukwu
 */
public class Task implements Serializable {

    private String runTitle;
    private String algorithmCode;
    private String algorithm;
    private String algorithmGroup; // graph, sort, hash, search
    private String taskID;
    private Long inputStartSize;
    private Long inputStepSize;
    private Long inputFinalSize;
    private Long inputMinValue;
    private Long inputMaxValue;
    private Integer numRuns;
    private Integer numRepeats;
    private Integer numCompletedRuns;
    private String inputDistribution;
    private String error;
    private String pivotPosition;
    private String inputFileName; // full path to custom input
    private String dataStructure;
    private String fixedGraphParam; // 0-vertices, 1-edges
    private Long fixedGraphSize;
    private Boolean isDirectedGraph;
    private Boolean allowSelfLoops;
    private Integer hashBucketSize;
    private String hashFunctionType; // good/bad hashing
    private String hashKeyType;
    private boolean graphIsDelayed;

    protected Integer status;
    // make it able to fire property changed events
    PropertyChangeSupport taskPcs;

    public Task() {
        this.status = 0;
        this.taskPcs = new PropertyChangeSupport(this);
        this.error = "";
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public String getTaskID() {
        return taskID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getInputFinalSize() {
        return inputFinalSize;
    }

    public void setInputFinalSize(Long inputFinalSize) {
        this.inputFinalSize = inputFinalSize;
    }

    public PropertyChangeSupport getTaskPcs() {
        return taskPcs;
    }

    public void setTaskPcs(PropertyChangeSupport taskPcs) {
        this.taskPcs = taskPcs;
    }

    public String getAlgorithmCode() {
        return algorithmCode;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public String getAlgorithmShortName() {
        return AlgoBench.properties.getProperty(algorithm.toUpperCase() + "_SHORT");
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm.replaceAll("\\s+", "_").toUpperCase();
        this.algorithmCode = AlgoBench.properties.getProperty(this.algorithm);
    }

    public Long getInputStartSize() {
        return inputStartSize;
    }

    public Boolean setInputStartSize(Long inputStartSize) {
        if (inputStartSize < 0) {
            this.error += "Please use only positive values.\n";
            return false;
        }
        this.inputStartSize = inputStartSize;
        return true;
    }

    public Long getInputStepSize() {
        return inputStepSize;
    }

    public Integer getNumRepeats() {
        return numRepeats;
    }

    public void setNumRepeats(Integer numRepeats) {
        this.numRepeats = numRepeats;
    }

    public Integer getNumCompletedRuns() {
        return numCompletedRuns;
    }

    public void setNumCompletedRuns(Integer numCompletedRuns) {
        Integer oldValue = this.numCompletedRuns;
        this.numCompletedRuns = numCompletedRuns;
        taskPcs.firePropertyChange("numCompletedRuns",
                oldValue, numCompletedRuns);
    }

    public boolean getGraphIsDelayed() {
        return graphIsDelayed;
    }

    public void setGraphIsDelayed(boolean graphIsDelayed) {
        this.graphIsDelayed = graphIsDelayed;
    }

    public Boolean getIsDirectedGraph() {
        return isDirectedGraph;
    }

    public void setIsDirectedGraph(Boolean isDirectedGraph) {
        this.isDirectedGraph = isDirectedGraph;
    }

    public Boolean getAllowSelfLoops() {
        return allowSelfLoops;
    }

    public void setAllowSelfLoops(Boolean allowSelfLoops) {
        this.allowSelfLoops = allowSelfLoops;
    }

    public String getFixedGraphParam(Boolean asString) {
        if (!asString) {
            return AlgoBench.properties.getProperty(fixedGraphParam.toUpperCase());
        }
        return fixedGraphParam;
    }

    public void setFixedGraphParam(String fixedGraphParam) {
        this.fixedGraphParam = fixedGraphParam;
    }

    public Long getFixedGraphSize() {
        return fixedGraphSize;
    }

    public String getAlgorithmGroup(Boolean asString) {
        if (!asString) {
            return AlgoBench.properties.getProperty(algorithmGroup.toUpperCase());
        }
        return algorithmGroup;
    }

    public void setAlgorithmGroup(String algorithmGroup) {
        this.algorithmGroup = algorithmGroup;
    }

    public void setFixedGraphSize(Long fixedGraphSize) {
        this.fixedGraphSize = fixedGraphSize;
    }

    public void setDataStructure(String dataStructure) {
        this.dataStructure = dataStructure;
    }

    public String getDataStructure(Boolean asString) {
        if (!asString) {
            String s = dataStructure.toUpperCase().replace(" ", "_");
            return AlgoBench.properties.getProperty("GRAPH_" + s);
        }
        return dataStructure;
    }

    public void setInputStepSize(Long inputStepSize) {
        if (inputStepSize == 0) {
            this.error += "Please use only non-zero values for step size.\n";
            return;
        }
        this.inputStepSize = inputStepSize;
    }

    public Long getInputMinValue() {
        return inputMinValue;
    }

    public void setInputMinValue(String inputMinValue) {
        this.inputMinValue = Long.parseLong(inputMinValue);
        if (this.inputMinValue > Long.MAX_VALUE
                || this.inputMinValue < 0) {
            this.error += "Invalid min value\n";
        }
    }

    public Long getInputMaxValue() {
        return inputMaxValue;
    }

    public void setInputMaxValue(String inputMaxValue) {
        this.inputMaxValue = Long.parseLong(inputMaxValue);
        if (this.inputMaxValue < this.inputMinValue) {
            this.error += "Max value is less than min value.\n";
        }
        if (this.inputMaxValue == 0) {
            this.error += "Invalid max value\n";
        }
    }

    public Integer getNumRuns() {
        return numRuns;
    }

    public void setNumRuns(Integer numRuns) {
        this.numRuns = numRuns;
    }

    public void setNumRuns() {
        if (this.inputStepSize == 0) {
            this.numRuns = 1;
            return;
        }
        Long temp = (this.inputFinalSize - this.inputStartSize) / this.inputStepSize;
        this.numRuns= temp.intValue();
        this.numRuns++; // add 1 for the running with the initial size
    }

    public String getInputFileName() {
        if (inputFileName == null) {
            return "";
        }
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    public String getInputDistribution() {
        return AlgoBench.properties.getProperty("INPUT_" + inputDistribution.toUpperCase());
    }

    public String getInputDistributionAsString() {
        return this.inputDistribution;
    }

    public void setInputDistribution(String inputDistribution) {
        this.inputDistribution = inputDistribution;
    }

    public void setRunTitle(String runTitle) {
        this.runTitle = runTitle;
    }

    public String getPivotPosition(boolean asString) {
        if (!asString) {
            return AlgoBench.properties.getProperty(
                    "PIVOT_" + pivotPosition.toUpperCase(), "1");
        }
        return pivotPosition;
    }

    public void setPivotPosition(String pivotPosition) {
        this.pivotPosition = pivotPosition;
    }

    public Integer getHashBucketSize() {
        return hashBucketSize;
    }

    public void setHashBucketSize(Integer hashBucketSize) {
        this.hashBucketSize = hashBucketSize;
    }

    public String getHashType(Boolean asString) {
        if (!asString) {
            return AlgoBench.properties.getProperty("HASHING_FUNCTION_" + hashFunctionType.toUpperCase());
        }
        return hashFunctionType;
    }

    public void setHashFunctionType(String hashFunctionType) {
        this.hashFunctionType = hashFunctionType;
    }

    public String getHashKeyType(Boolean asString) {
        if (!asString) {
            return AlgoBench.properties.getProperty("HASHING_KEY_" + hashKeyType.toUpperCase());
        }
        return hashKeyType;
    }

    public void setHashKeyType(String hashKeyType) {
        this.hashKeyType = hashKeyType;
    }

    public String getRunTitle() {
        return runTitle;
    }

    public String getError() {
        return this.error;
    }

    public void logError(String message) {
        this.error += message;
    }

    public void clearErrorLog() {
        this.error = "";
    }

    @Override
    public String toString() {
        return runTitle;
    }

    public String getCommand() {
        String r = "ALGORITHM:" + getAlgorithmCode();
        r += "\nALGORITHM-GROUP:" + getAlgorithmGroup(false);
        r += "\nINPUT-STARTSIZE:" + getInputStartSize();
        r += "\nINPUT-STEPSIZE:" + getInputStepSize();
        r += "\nINPUT-FILENAME:" + getInputFileName();
        r += "\nNUMRUNS:" + getNumRuns();
        r += "\nNUMREPEATS:" + getNumRepeats();
        if (getAlgorithmGroup(true).equals("GRAPH")) {
            r += "\nGRAPH-STRUCTURE:" + getDataStructure(false);
            r += "\nGRAPH-FIXED-SIZE:" + getFixedGraphSize();
            r += "\nGRAPH-FIXED-EDGES:" + getFixedGraphParam(false);
            r += "\nGRAPH-ALLOW-SELF-LOOP:" + (getAllowSelfLoops() ? "1" : "0");
            r += "\nGRAPH-IS-DIRECTED:" + (getIsDirectedGraph() ? "1" : "0");
            r += "\nGRAPH-IS-DELAYED:" + (getGraphIsDelayed() ? "1" : "0");
        }
        else if (getAlgorithmGroup(true).equals("HASH")) {
            r += "\nHASH-BUCKET-ARRAY-SIZE:" + Integer.toString(getHashBucketSize());
            r += "\nHASH-FUNCTION-TYPE:" + getHashType(false);
            r += "\nHASH-KEY-TYPE:" + getHashKeyType(false);
        }
        else {
            r += "\nINPUT-MINVALUE:" + getInputMinValue();
            r += "\nINPUT-MAXVALUE:" + getInputMaxValue();
            r += "\nINPUT-DISTRIBUTION:" + getInputDistribution();

            if (getAlgorithmCode().equals(AlgoBench.properties.getProperty("QUICKSORT"))) {
                r += "\nQUICKSORT-PIVOT-POSITION:" + getPivotPosition(false);
            }
        }
        r += "\n";
        return r;
    }

    public void update(String response) {
        // parse the response
        String[] parts = response.split(":");
        switch (parts[0]) {
            case "NUMCOMPLETEDRUNS":
                setNumCompletedRuns(Integer.parseInt(parts[1]));
                break;
        }
    }

    public void
            addPropertyChangeListener(PropertyChangeListener listener) {
        taskPcs.addPropertyChangeListener(listener);
    }

    public void
            removePropertyChangeListener(PropertyChangeListener listener) {
        taskPcs.removePropertyChangeListener(listener);
    }

}
